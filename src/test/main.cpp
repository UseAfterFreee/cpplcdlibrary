#include <iostream>
#include <string>
#include <unistd.h>

#include <wiringPi.h>

#include "../lib/lcd_display.hpp"

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        std::cout << "Usage " << argv[0] << " TEXT" << std::endl;
        exit(1);
    }

    std::string s(argv[1]);

    std::cout << "Initializing wiringPi" << std::endl;

    wiringPiSetupGpio();

    std::cout << "Initializing LCD object" << std::endl;    
    
    LCD lcd(17, 27, 5, 6, 13, 19);

    std::cout << "[LOG]: Printing \"" << s << "\"" << std::endl;
    lcd.print(s);

    sleep(10);
    lcd.displayoff();
}
