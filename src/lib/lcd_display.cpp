#include "lcd_display.hpp"

#include <unistd.h>
#include <wiringPi.h>

LCD::LCD(uint8_t rs, uint8_t enable, uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3)
    :   mRSPin(rs),
        mRWPin(255),
        mEnablePin(enable),
        mDataPins{d0, d1, d2, d3, 0, 0, 0, 0},
        mDisplayFunction(0x00 | 0x00 | 0x00),
        mNumberOfLines(1),
        mRowOffsets{0x00, 0x40, 0x00 + 16, 0x40 + 16},
        mDisplayControl(0x04 | 0x00 | 0x00),
        mDisplayMode(0x02 | 0x00)
{
    // begin(16, 1)
    pinMode(mRSPin, OUTPUT); 
    pinMode(mEnablePin, OUTPUT); 

    for (int i = 0; i<4; ++i)
    {
        pinMode(mDataPins[i], OUTPUT);
    }

    usleep(50000);

    digitalWrite(mRSPin, LOW);
    digitalWrite(mEnablePin, LOW);
    
    const unsigned int tries = 3;
    for (unsigned int i = 0; i<tries; ++i)
    {
        write4bits(0x03);
        usleep(4500);
    }
    write4bits(0x02);

    send(0x20 | mDisplayFunction, COMMAND);

    display();

    clear();

    send(0x04 | mDisplayMode, COMMAND);
}

void LCD::pulseEnable()
{
    digitalWrite(mEnablePin, LOW);
    usleep(1);
    digitalWrite(mEnablePin, HIGH);
    usleep(1);
    digitalWrite(mEnablePin, LOW);
    usleep(100);
}

void LCD::write4bits(uint8_t value)
{
    for (int i = 0; i<4; ++i)
    {
        digitalWrite(mDataPins[i], (value >> i) & 0x01);
    }

    pulseEnable();
}

void LCD::send(uint8_t value, uint8_t mode)
{
    digitalWrite(mRSPin, mode);
    if (mRWPin != 255)
    {
        digitalWrite(mRWPin, LOW);
    }

    write4bits(value>>4);
    write4bits(value);
}

void LCD::display()
{
    mDisplayControl |= 0x04;
    send(0x08 | mDisplayControl, COMMAND);
}

void LCD::displayoff()
{
    mDisplayControl &= ~0x04;
    send(0x08 | mDisplayControl, COMMAND);
}

void LCD::clear()
{
    send(0x01, COMMAND);
    usleep(2000);
}

void LCD::print(std::string s)
{
    for (char &c : s) send(c, WRITE);
}
