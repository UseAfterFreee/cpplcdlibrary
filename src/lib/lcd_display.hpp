#ifndef SRC_LIB_LCD_DISPLAY_HPP
#define SRC_LIB_LCD_DISPLAY_HPP

#include <cstdint>
#include <string>

#define COMMAND 0
#define WRITE 1

class LCD
{
 private:
     uint8_t mRSPin;
     uint8_t mRWPin;
     uint8_t mEnablePin;
     uint8_t mDataPins[8];
     uint8_t mDisplayFunction;
     uint8_t mNumberOfLines;
     uint8_t mRowOffsets[4];
     uint8_t mDisplayControl;
     uint8_t mDisplayMode;

     void pulseEnable();
     void write4bits(uint8_t value);

     void send(uint8_t value, uint8_t mode);
 public:
     LCD(uint8_t rs,
         uint8_t enable,
         uint8_t d0,
         uint8_t d1,
         uint8_t d2,
         uint8_t d3);

     void display();
     void displayoff();
     void clear();

     void print(std::string s);
}; 

#endif  // SRC_LIB_LCD_DISPLAY_HPP
