CC=g++

TEST_SRC=$(wildcard ./src/test/*.cpp)
LIB_SRC=$(wildcard ./src/lib/*.cpp)
TEST_HDR=$(wildcard ./src/test/*.hpp)
LIB_HDR=$(wildcard ./src/lib/*.hpp)

TEST_OBJ=$(patsubst ./src/test/%.cpp,./obj/%.o,$(TEST_SRC))
LIB_OBJ=$(patsubst ./src/lib/%.cpp,./obj/%.o,$(LIB_SRC))

TEST_EXE=./bin/test

.PHONY: debug clean

debug: $(TEST_EXE)

$(TEST_EXE): $(TEST_OBJ) $(LIB_OBJ)
	$(CC) -g -O0 -o $@ $^ -lwiringPi

./obj/%.o: ./src/test/%.cpp
	$(CC) -g -O0 -c -o $@ $<

./obj/%.o: ./src/lib/%.cpp
	$(CC) -g -O0 -c -o $@ $<

clean:
	rm ./bin/*
	rm ./obj/*
